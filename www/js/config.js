angular
  .module('app.config', [])
  .config(config);

  function config($logProvider, $httpProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    $httpProvider.defaults.useXDomain = true;
    console.log('interceptors aaa')
    $httpProvider.interceptors.push(
      function ($q, $window) {
        return {
          request: function(config) { 
            config.headers = config.headers || {};  
            config.headers['Content-Type'] = 'application/json';
            config.headers['Accept'] = 'json' 
            if ($window.sessionStorage.getItem('user')) {
              config.headers['X-Token'] = $window.sessionStorage.getItem('user');
            }           
            return config;
          }
        };
      }
    );
  };
