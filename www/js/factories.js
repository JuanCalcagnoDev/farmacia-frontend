angular.module('app.factories',[])

.factory('UserId', [function(){
    console.log('entrando a user id')
    var savedUser = {}
     function set(data) {
    console.log('grabando')
       savedUser = data;
     }
     function get() {
    console.log('retirando')
      return savedUser;
     }

     return {
      set: set,
      get: get
     }
}])

.factory('PharmacyId', [function(){
    
    var savedPharmacy = {}
     function set(data) {
       savedPharmacy = data;
     }
     function get() {
      return savedPharmacy;
     }

     return {
      set: set,
      get: get
     }
}])

  .factory('City',function (API,$resource) {
  return $resource(API.cities,{id: '@id'},{})
})
  .factory('User',function (API,$resource) {
  return $resource(API.user,{id: '@id'},{})
})
  .factory('Pharmacies',function (API,$resource) {
  return $resource(API.pharmacies,{id: '@id'},{})
})
  .factory('CategoryProduct',function (API,$resource) {
  return $resource(API.categoryProduct,{pharmacy_id: '@pharmacy_id',category_id: '@category_id'},{})
})
  .factory('AllProducts',function (API,$resource) {
  return $resource(API.allProductsByPharmacy,{id: '@id'},{})
})
  .factory('Products',function (API,$resource) {
  return $resource(API.products,{id: '@id'},{})
})
  .factory('Categories',function (API,$resource) {
  return $resource(API.categories,{id: '@id'},{})
})
  .factory('PharmaciesByProduct',function (API,$resource) {
  return $resource(API.pharmaciesByProduct ,{id: '@id'},{})
})
  .factory('ProductsByPharmacy',function (API,$resource) {
  return $resource(API.productsByPharmacies ,{id: '@id'},{})
})
  .factory('Purchase',function (API,$resource) {
  return $resource(API.bill ,{user_id: '@user_id'},{})
})
  .factory('Donate',function (API,$resource) {
  return $resource(API.donate ,{id: '@id'},{})
})
  .factory('Fundations',function (API,$resource) {
  return $resource(API.fundations ,{id: '@id'},{})
})

