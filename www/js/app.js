// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic','app.config','app.controllers', 'app.routes', 'app.services', 'app.directives','app.factories','app.constants','ngResource','ngStorage'])
  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if(window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })
   .run(runBlock);

    /** @ngInject */
     function runBlock($log, $rootScope, $state) {

     $rootScope.$on('$stateChangeError', 
       function (event, toState, toParams, fromState, fromParams, error) {
         if (angular.isString(error)) {
               event.preventDefault();
               switch (error) {
                 case 'NotAuthorized':
                   $state.go('anon.login');
                   break;
                 case 'NotAnon':
                   $state.go('menu.home');
                   break;
               }
           }
       }
     );

       $log.debug('runBlock end');
     };

