angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider){

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js



  $stateProvider
  .state('anon', {
    abstract: true,
    template: '<ion-nav-view></ion-nav-view>',
     resolve: {
       logged: function($q, $injector){
         if ($injector.get('Token').has()) {
           console.log('mamalo')
           return $q.reject('NotAnon');

         }
       }
     }
  })
  .state('anon.signup', {
    url: '/page5',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('anon.login', {
    url: '/page4',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl',
    abstract:true,
        resolve: {
          security: function($q, $injector){
            if (!$injector.get('Token').has()) {
              return $q.reject('NotAuthorized');
            }
          }
        }
  })

  .state('menu.home', {
    url: '/page1',
    views: {
      'side-menu21': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })

  .state('menu.donaciones', {
    url: '/page6',
    views: {
      'side-menu21': {
        templateUrl: 'templates/donaciones.html',
        controller: 'donacionesCtrl'
      }
    }
  })

  .state('menu.compras', {
    url: '/page8',
    views: {
      'side-menu21': {
        templateUrl: 'templates/compras.html',
        controller: 'comprasCtrl'
      }
    }
  })


  .state('menu.farmacias', {
    url: '/page10/',
    params:{
     id:''
   },
   views: {
    'side-menu21': {
      templateUrl: 'templates/farmacias.html',
      controller: 'farmaciasCtrl'
    }
  },
  resolve: {
    farmacias: function (Pharmacies) {
        return Pharmacies.query().$promise;
    }
  }
})  
  .state('menu.ciudad', {
    url: '/page15/',
    views: {
      'side-menu21': {
        templateUrl: 'templates/ciudad.html',
        controller: 'ciudadCtrl'
      }
    },
    resolve:{
      ciudades: function (City) {
        return City.query().$promise;
      }
    }
  }) 

  .state('menu.categorias', {
    url: '/page13/:id',
    params:{
     id:''
   },
   views: {
    'side-menu21': {
      templateUrl: 'templates/categorias.html',
      controller: 'categoriasCtrl'
    }
  },
  resolve:{
    categorias: function (Categories,$stateParams) {
      return Categories.query({id:$stateParams.id})
    }
  }
})

  .state('menu.productos', {
    url: '/page12/',
    params:{
      category_id:'',
      pharmacy_id:''
    },
    views: {
      'side-menu21': {
        templateUrl: 'templates/productos.html',
        controller: 'productosCtrl'
      }
    }
  })



  $urlRouterProvider.otherwise('/page4')

  

});