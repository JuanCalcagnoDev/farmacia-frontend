
angular.module('app.controllers', [])
  
.controller('homeCtrl', function($scope,Products,PharmaciesByProduct) {
			 
			 $scope.productos =  Products.query();

			 $scope.getPharmacies = function (index) {
			 	$scope.pharmacies = PharmaciesByProduct.query({id: index});		 	
			 }
})
   
.controller('cartCtrl', function($scope) {

})
   
.controller('cloudCtrl', function($scope) {

})
      
.controller('loginCtrl', function($scope,$state,$http,$window,UserId) {
	
	$scope.user = {};

	$scope.access = function () {
	console.log('LOGEANADO')
	$http({
			method: 'POST',
			url: 'http://192.168.0.112:3000/access',
			data:$scope.user
		}).success(function (data) {

			$window.sessionStorage.setItem('user', data.token);
			$state.go('menu.home')
			UserId.set(data.id);
			console.log(data.id);

		})
	};

})
   
   
.controller('farmaciasCtrl', function($scope,farmacias) {
	$scope.farmacias = farmacias;
})
   
.controller('ciudadCtrl', function($scope,$http,ciudades) {
		$scope.cities = ciudades;
})

.controller('categoriasCtrl', function($scope,$stateParams,$http,PharmacyId,Categories,categorias) {

		$scope.categorias = categorias;
		PharmacyId.set($stateParams.id)
})

.controller('productosCtrl', function($scope,$stateParams,$state,$http,PharmacyId,CategoryProduct,ProductsByPharmacy,Purchase,UserId) {
			
			$scope.pharmacy_id = PharmacyId.get();
			$scope.user_id     = UserId.get();
		if ($stateParams.category_id) {
			 $scope.productos = CategoryProduct.query({pharmacy_id: $scope.pharmacy_id, category_id: $stateParams.category_id}, function (data){
			 	$scope.productos = data;
			 });
		}
		else{
			 $scope.productos = ProductsByPharmacy.query({id:$scope.pharmacy_id}, function (data) {
			 	$scope.productos = data;
			 });		
		}
			$scope.bills = [];

      $scope.bill = {
        user_id:  parseInt($scope.user_id),
        pharmacy_id: parseInt($scope.pharmacy_id),
        bill_details_attributes:[{}]
      }

    $scope.purchase = function (id) {
      $scope.bill.bill_details_attributes[0].product_id = id;
      console.log($scope.bill)
      $scope.bills.push(Purchase.save($scope.bill));
    };
		

})

.controller('signupCtrl', function($scope , $http) {
	$scope.user = {};

	$scope.createUser = function () {
	
	$http({
			method: 'POST',
			url: 'http://192.168.0.112:3000/users',
			data:$scope.user
		}).success(function (data) {
			
		})
	};


})
   
.controller('donacionesCtrl', function($scope,Donate,UserId,Fundations) {
	
		console.log('donaciones ctrl')
		$scope.user_id     = UserId.get();
		console.log($scope.user_id)
		$scope.fundations  = Fundations.query(); 


		$scope.donations   = [];

		$scope.donation = {
      user_id:  parseInt($scope.user_id),
		}

		$scope.donate = function () {
						console.log($scope.user_id)
			
			for (var i = 0; i < $scope.fundations.length; i++) {
			
				if ($scope.fundations[i].selected == true) {
						console.log($scope.fundations[i].name)
						$scope.donation.fundation_id = $scope.fundations[i].id
						console.log($scope.donation);
						$scope.donations.push(Donate.save($scope.donation));
				}

			}

		}

})
   
.controller('menuCtrl', function($scope,$window) {
	$scope.logout = function () {
		 $window.sessionStorage.removeItem('user');
	}

})


