angular.module('app.services', [])
// .service('Token', [function($window){

//     this.set = function (user) {
//       $window.sessionStorage.setItem('user', JSON.stringify(user));
//     };

//     this.get = function () {
//       return JSON.parse($window.sessionStorage.getItem('user'));
//     };

//     this.has = function () {
//       console.log('entrando a token');
//       var x =  $window.sessionStorage.getItem('user');
//       return !!$window.sessionStorage.getItem('user');
//     }
// }])
  
  .service('Token', function ($window) {

    this.set = function (user) {
      $window.sessionStorage.setItem('user', JSON.stringify(user));
    };

    this.get = function () {
      return JSON.parse($window.sessionStorage.getItem('user'));
    };

    this.has = function () {
      return !!$window.sessionStorage.getItem('user');
    }

  })
.service('API', [function(){
  var baseUrl =  'http://192.168.0.112:3000'
  var toUrl = function (path) {
    return baseUrl+path;
  }
  this.user                        = toUrl('/users');
  this.bill                        = toUrl('/users/:user_id/bills/');
  this.cities                      = toUrl('/cities');
  this.pharmacies                  = toUrl('/pharmacies');
  this.categoryProduct             = toUrl('/pharmacies/:pharmacy_id/categories/:category_id/products');
  this.allProductsByPharmacy       = toUrl('/pharmacies/:id/products/');
  this.categories                  = toUrl('/pharmacies/:id/categories');
  this.productsByPharmacies        = toUrl('/pharmacies/:id/products');
  this.products                    = toUrl('/products/:id/');
  this.pharmaciesByProduct         = toUrl('/products/:id/pharmacies');
  this.donate                      = toUrl('/donations');
  this.fundations                  = toUrl('/fundations');

}])

